-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2018 at 09:08 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobtel`
--

-- --------------------------------------------------------

--
-- Table structure for table `marka`
--

CREATE TABLE `marka` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marka`
--

INSERT INTO `marka` (`id`, `name`) VALUES
(1, 'Samsung'),
(2, 'HTC');

-- --------------------------------------------------------

--
-- Table structure for table `mobilni`
--

CREATE TABLE `mobilni` (
  `id` int(11) NOT NULL,
  `id_vrsta` int(11) NOT NULL,
  `serijskiBroj` varchar(15) NOT NULL,
  `visina` double NOT NULL,
  `sirina` double NOT NULL,
  `duzina` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobilni`
--

INSERT INTO `mobilni` (`id`, `id_vrsta`, `serijskiBroj`, `visina`, `sirina`, `duzina`) VALUES
(1, 1, 'D4300', 143.8, 68.2, 9),
(2, 1, '1890E', 145, 34, 7),
(3, 2, '4C300', 133.6, 56.2, 4),
(5, 4, 'A345', 153, 135, 3);

-- --------------------------------------------------------

--
-- Table structure for table `vrsta`
--

CREATE TABLE `vrsta` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `id_marka` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vrsta`
--

INSERT INTO `vrsta` (`id`, `name`, `id_marka`) VALUES
(1, 'S8', 1),
(2, 'S9', 1),
(3, 'J6', 1),
(4, '10', 2),
(5, 'ULTRA U', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `marka`
--
ALTER TABLE `marka`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobilni`
--
ALTER TABLE `mobilni`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vrsta` (`id_vrsta`);

--
-- Indexes for table `vrsta`
--
ALTER TABLE `vrsta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_marka` (`id_marka`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `marka`
--
ALTER TABLE `marka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mobilni`
--
ALTER TABLE `mobilni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `vrsta`
--
ALTER TABLE `vrsta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mobilni`
--
ALTER TABLE `mobilni`
  ADD CONSTRAINT `mobilni_ibfk_1` FOREIGN KEY (`id_vrsta`) REFERENCES `vrsta` (`id`);

--
-- Constraints for table `vrsta`
--
ALTER TABLE `vrsta`
  ADD CONSTRAINT `vrsta_ibfk_1` FOREIGN KEY (`id_marka`) REFERENCES `marka` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
