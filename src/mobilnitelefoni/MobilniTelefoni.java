/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobilnitelefoni;

import database.DB;
import entities.Marka;
import entities.Mobilni;
import entities.Vrsta;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MobilniTelefoni extends Application {

    ComboBox<Marka> marka = new ComboBox<>();
    ComboBox<Vrsta> vrsta = new ComboBox<>();

    TextField serijskiBroj = new TextField();
    TextField visina = new TextField();
    TextField sirina = new TextField();
    TextField duzina = new TextField();

    Button saveOrUpdate = new Button("Sacuvaj");
    Button delete = new Button("Obrisi");

    TableView<Mobilni> table = new TableView<>();
    Mobilni changeableMobilni = new Mobilni();

    @Override
    public void start(Stage primaryStage) throws SQLException {
        marka.setPromptText("Marka");

        vrsta.setPromptText("Vrsta");
        serijskiBroj.setPromptText("Serijski broj");
        visina.setPromptText("Visina");
        sirina.setPromptText("Sirina");
        duzina.setPromptText("Duzina");

        marka.setItems(FXCollections.observableList(DB.database.getAllFromMarka()));

        BorderPane root = new BorderPane(table);

        VBox leftBox = new VBox(marka, vrsta, serijskiBroj, visina, sirina, duzina, saveOrUpdate, delete);
        root.setLeft(leftBox);
        leftBox.setSpacing(15);
        leftBox.setPadding(new Insets(15));

        Scene scene = new Scene(root, 630, 370);

        primaryStage.setTitle("Mobilni telefoni");
        primaryStage.setScene(scene);
        primaryStage.show();

        marka.getSelectionModel().selectedItemProperty().addListener(e -> {
            if (marka.getSelectionModel().getSelectedItem() != null) {
                vrsta.setItems(FXCollections.observableList(
                        marka.getSelectionModel().getSelectedItem().getVrste()));
            }

        });
        saveOrUpdate.setOnAction(e -> {
            if (saveOrUpdate.getText().equals("Sacuvaj")) {
                try {
                    Mobilni m = new Mobilni();
                    m.setMarka(marka.getSelectionModel().getSelectedItem());
                    m.setVrsta(vrsta.getSelectionModel().getSelectedItem());
                    m.setSerijskiBroj(serijskiBroj.getText());
                    m.setVisina(Double.parseDouble(visina.getText()));
                    m.setSirina(Double.parseDouble(sirina.getText()));
                    m.setDuzina(Double.parseDouble(duzina.getText()));

                    DB.database.insertMobilni(m);
                    serijskiBroj.clear();
                    visina.clear();
                    sirina.clear();
                    duzina.clear();

                    vrsta.getSelectionModel().clearSelection();
                    marka.getSelectionModel().clearSelection();

                    table.getItems().add(m);
                    table.requestLayout();
                    util.Util.showInfoAlert("Uspesno ste dodali mobilni telefon");
                } catch (NumberFormatException ex) {
                    util.Util.showErrorAlert("Neuspesan unos");
                } catch (SQLException ex) {
                    Logger.getLogger(MobilniTelefoni.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                if (vrsta.getSelectionModel().getSelectedItem() != null) {
                    changeableMobilni.setMarka(marka.getSelectionModel().getSelectedItem());
                    changeableMobilni.setVrsta(vrsta.getSelectionModel().getSelectedItem());
                    changeableMobilni.setSerijskiBroj(serijskiBroj.getText());
                    changeableMobilni.setVisina(Double.parseDouble(visina.getText()));
                    changeableMobilni.setSirina(Double.parseDouble(sirina.getText()));
                    changeableMobilni.setDuzina(Double.parseDouble(duzina.getText()));

                    try {
                        DB.database.updateMobilni(changeableMobilni);
                    } catch (SQLException ex) {
                        Logger.getLogger(MobilniTelefoni.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    serijskiBroj.clear();
                    visina.clear();
                    sirina.clear();
                    duzina.clear();

                    vrsta.getSelectionModel().clearSelection();
                    marka.getSelectionModel().clearSelection();
                    saveOrUpdate.setText("Sacuvaj");
                    table.refresh();
                    util.Util.showInfoAlert("Uspesno ste izmenili mobilni");

                }
            }
        });

        table.setItems(FXCollections.observableList(DB.database.getAllFromMobilni()));
        TableColumn<Mobilni, String> col1 = new TableColumn<>("Marka");
        col1.setCellValueFactory((e) -> {
            return new SimpleStringProperty(e.getValue().getMarka().getName());
        });
        TableColumn<Mobilni, String> col2 = new TableColumn<>("Vrsta");
        col2.setCellValueFactory((e) -> {
            return new SimpleStringProperty(e.getValue().getVrsta().getName());
        });
        TableColumn<Mobilni, String> col3 = new TableColumn<>("Serijski broj");
        col3.setCellValueFactory((e) -> {
            return new SimpleStringProperty(e.getValue().getSerijskiBroj() + "");
        });

        TableColumn<Mobilni, String> col4 = new TableColumn<>("Visina");
        col4.setCellValueFactory((e) -> {
            return new SimpleStringProperty(e.getValue().getVisina() + "");
        });
        TableColumn<Mobilni, String> col5 = new TableColumn<>("Sirina");
        col5.setCellValueFactory((e) -> {
            return new SimpleStringProperty(e.getValue().getSirina() + "");
        });
        TableColumn<Mobilni, String> col6 = new TableColumn<>("Duzina");
        col6.setCellValueFactory((e) -> {
            return new SimpleStringProperty(e.getValue().getDuzina() + "");
        });

        delete.setOnAction(e -> {
            Mobilni m = table.getSelectionModel().getSelectedItem();
            if (m == null) {
                util.Util.showErrorAlert("Niste selektovali nista");
            } else {
                try {
                    DB.database.deleteFromMobilni(m);
                } catch (SQLException ex) {
                    Logger.getLogger(MobilniTelefoni.class.getName()).log(Level.SEVERE, null, ex);
                }
                table.getItems().remove(m);
                serijskiBroj.clear();
                visina.clear();
                sirina.clear();
                duzina.clear();

                vrsta.getSelectionModel().clearSelection();
                marka.getSelectionModel().clearSelection();
                table.requestLayout();
                util.Util.showInfoAlert("Uspesno obrisano");
            }
            saveOrUpdate.setText("Sacuvaj");
        });
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.getColumns().setAll(col1, col2, col3, col4, col5, col6);

        table.getSelectionModel().selectedItemProperty().addListener(e -> {
            changeableMobilni = table.getSelectionModel().getSelectedItem();
            marka.getSelectionModel().select(changeableMobilni.getMarka());
            vrsta.getSelectionModel().select(changeableMobilni.getVrsta());
            serijskiBroj.setText(changeableMobilni.getSerijskiBroj());
            visina.setText(changeableMobilni.getVisina() + "");
            sirina.setText(changeableMobilni.getSirina() + "");
            duzina.setText(changeableMobilni.getDuzina() + "");

            saveOrUpdate.setText("Update");

        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
