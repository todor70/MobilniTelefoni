/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import com.mysql.jdbc.Statement;
import entities.Mobilni;
import entities.Marka;
import entities.Vrsta;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DB {

    public static DB database = new DB();
    private String urlWithoutDatabase = "jdbc:mysql://localhost:3306/";
    private String url = "jdbc:mysql://localhost:3306/mobtel";
    private String username = "root";
    private String password = "";

    private Connection con;

    public DB() {
        if (database == null) {
            try {
                createDatabase();

                createTable("create table if not exists marka("
                        + "id int auto_increment primary key,"
                        + "name varchar(15) not null"
                        + ")");
                createTable("create table if not exists vrsta("
                        + "id int auto_increment primary key,"
                        + "name varchar(15) not null,"
                        + "id_marka int not null,"
                        + "foreign key(id_marka) references marka(id)"
                        + ")");

                createTable("create table if not exists mobilni("
                        + "id int auto_increment primary key,"
                        + "id_vrsta int not null,"
                        + "serijskiBroj varchar(15) not null,"
                        + "visina double not null,"
                        + "sirina double not null,"
                        + "duzina double not null,"
                        + "foreign key(id_vrsta) references vrsta(id)"
                        + ")");

            } catch (SQLException ex) {
                ex.printStackTrace();
                System.exit(0);
            }
        }
    }

    public void createDatabase() throws SQLException {
        con = DriverManager.getConnection(urlWithoutDatabase,
                username, password);
        PreparedStatement stmt = con.prepareStatement("create database"
                + " if not exists mobtel");
        stmt.execute();
        con.close();
    }

    public void openConnection() throws SQLException {
        con = DriverManager.getConnection(url, username, password);
    }

    public void closeConnection() throws SQLException {
        con.close();
    }

    public void createTable(String sql) throws SQLException {
        openConnection();
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.execute();
        closeConnection();
    }

    public List<Marka> getAllFromMarka() throws SQLException {
        openConnection();
        PreparedStatement stmt = con.prepareStatement("select * from marka order by name asc");
        ResultSet set = stmt.executeQuery();
        List<Marka> list = new ArrayList<>();

        while (set.next()) {
            Marka m = new Marka(set.getInt("id"),
                    set.getString(2));
            m.setVrste(getFromVrstaByMarka(m.getId()));
            list.add(m);
        }
        closeConnection();
        return list;

    }

    private List<Vrsta> getFromVrstaByMarka(long id) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("select * from vrsta where "
                + "id_marka = ? order by name asc");
        stmt.setLong(1, id);
        ResultSet set = stmt.executeQuery();
        List<Vrsta> vrste = new ArrayList<>();

        while (set.next()) {
            vrste.add(new Vrsta(set.getInt(1), set.getString(2)));
        }
        return vrste;

    }

    public void insertMobilni(Mobilni m) throws SQLException {
        openConnection();
        PreparedStatement stmt = con.prepareStatement("insert into mobilni(id_vrsta, serijskiBroj, visina, sirina, duzina)"
                + " values(?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
        stmt.setLong(1, m.getVrsta().getId());
        stmt.setString(2, m.getSerijskiBroj());
        stmt.setDouble(3, m.getVisina());
        stmt.setDouble(4, m.getSirina());
        stmt.setDouble(5, m.getDuzina());

        stmt.execute();
        ResultSet set = stmt.getGeneratedKeys();

        while (set.next()) {
            m.setId(set.getLong(1));
        }

        closeConnection();
    }

    public List<Mobilni> getAllFromMobilni() throws SQLException {
        openConnection();
        PreparedStatement stmt = con.prepareStatement("SELECT mobilni.id as mobilni_id, "
                + "marka.id as marka_id, "
                + "marka.name as marka_name, vrsta.id as vrsta_id, "
                + "vrsta.name as vrsta_name, mobilni.serijskiBroj as mobilni_serijskiBroj, "
                + "mobilni.visina as mobilni_visina, mobilni.sirina as mobilni_sirina, "
                + "mobilni.duzina as mobilni_duzina FROM mobilni "
                + "LEFT JOIN vrsta on vrsta.id = mobilni.id_vrsta "
                + "LEFT JOIN marka on marka.id = vrsta.id_marka ");
        List<Mobilni> mob = new ArrayList<>();
        ResultSet set = stmt.executeQuery();
        while (set.next()) {
            mob.add(new Mobilni(set.getLong("mobilni_id"),
                    new Marka(set.getLong("marka_id"), set.getString("marka_name")),
                    new Vrsta(set.getLong("vrsta_id"), set.getString("vrsta_name")),
                    set.getString("mobilni_serijskiBroj"),
                    set.getDouble("mobilni_visina"),
                    set.getDouble("mobilni_sirina"),
                    set.getDouble("mobilni_duzina")));
        }
        closeConnection();
        return mob;
    }

    public void deleteFromMobilni(Mobilni m) throws SQLException {
        openConnection();
        PreparedStatement stmt = con.prepareStatement("delete from mobilni where id = ?");
        stmt.setLong(1, m.getId());

        stmt.executeUpdate();
        closeConnection();
    }

    public void updateMobilni(Mobilni m) throws SQLException {
        openConnection();
        PreparedStatement stmt = con.prepareStatement("update mobilni set id_vrsta=?, serijskiBroj= ?,"
                + " visina = ?, sirina = ?, duzina = ? where id = ?");

        stmt.setLong(1, m.getVrsta().getId());
        stmt.setString(2, m.getSerijskiBroj());
        stmt.setDouble(3, m.getVisina());
        stmt.setDouble(4, m.getSirina());
        stmt.setDouble(5, m.getDuzina());

        stmt.setLong(6, m.getId());

        stmt.executeUpdate();
        closeConnection();
    }
}
