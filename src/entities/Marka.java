/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import java.util.List;

//marka telefona
public class Marka {

    private long id;
    private String name;
    private List<Vrsta> vrste = new ArrayList<>();

    public Marka() {
    }

    public Marka(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Vrsta> getVrste() {
        return vrste;
    }

    public void setVrste(List<Vrsta> vrste) {
        this.vrste = vrste;
    }

    @Override
    public String toString() {
        return name;
    }

    
}
