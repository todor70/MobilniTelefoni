/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

public class Mobilni {

    private long id;
    private Marka marka;
    private Vrsta vrsta;
    private String serijskiBroj;
    private double visina;
    private double sirina;
    private double duzina;

    public Mobilni() {
    }

    public Mobilni(long id, Marka marka, Vrsta vrsta, String serijskiBroj, double visina, double sirina, double duzina) {
        this.id = id;
        this.marka = marka;
        this.vrsta = vrsta;
        this.serijskiBroj = serijskiBroj;
        this.visina = visina;
        this.sirina = sirina;
        this.duzina = duzina;
    }

  

    public String getSerijskiBroj() {
        return serijskiBroj;
    }

    public void setSerijskiBroj(String serijskiBroj) {
        this.serijskiBroj = serijskiBroj;
    }

   

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Marka getMarka() {
        return marka;
    }

    public void setMarka(Marka marka) {
        this.marka = marka;
    }

    public Vrsta getVrsta() {
        return vrsta;
    }

    public void setVrsta(Vrsta vrsta) {
        this.vrsta = vrsta;
    }

    public double getVisina() {
        return visina;
    }

    public void setVisina(double visina) {
        this.visina = visina;
    }

    public double getSirina() {
        return sirina;
    }

    public void setSirina(double sirina) {
        this.sirina = sirina;
    }

    public double getDuzina() {
        return duzina;
    }

    public void setDuzina(double duzina) {
        this.duzina = duzina;
    }

    @Override
    public String toString() {
        return "Mobilni{" + "id=" + id + ", marka=" + marka + ", vrsta=" + vrsta + ", serijskiBroj=" + serijskiBroj + ", visina=" + visina + ", sirina=" + sirina + ", duzina=" + duzina + '}';
    }

  
}
